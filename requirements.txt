django
djangorestframework
djangorestframework-camel-case
django-cors-headers
django-filter
django-phonenumber-field
django-countries
psycopg2-binary
pytz
idna
certifi
chardet
requests
python-stdnum
pillow

# Authentication


# Test Requirements
pytest
pytest-django
pytest-asyncio
behave
behave-django

# Async Django
asgiref
channels
channels-redis
aiohttp
cchardet
aiodns

# Tasks Queues
celery
django-celery-results
django-celery-beat
redis

# Development Requirements
ipython
drf-yasg
django-debug-toolbar
flake8
ipdb
faker
sentry-sdk==0.11.1

# Deployment Requirements
python-decouple
