from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions, authentication

swagger_info = openapi.Info(
    title="Dating App API",
    default_version=settings.STABLE_API_VERSION,
    description="""

        Documentation of Dating App open API

        """,
    terms_of_service="https://inchidi.id/tos/",
    contact=openapi.Contact(email="_@inchidi.id"),
    license=openapi.License(name="BSD License"),
)
schema_view = get_schema_view(
    info=swagger_info,
    public=False,
    permission_classes=(permissions.IsAuthenticated,),
    authentication_classes=(authentication.SessionAuthentication,)
)

urlpatterns = \
    [
        path('admin/', admin.site.urls, name='admin'),
        path('api/', include('core_api.urls'), name='api'),
        path('', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
        path('swagger/', schema_view.with_ui('swagger', cache_timeout=None), name='schema-swagger-ui'),
        re_path(
            r'^swagger(?P<format>.json|.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'
        ),
    ] + static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    ) + static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT
    )
